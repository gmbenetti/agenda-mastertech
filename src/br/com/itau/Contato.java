package br.com.itau;

public class Contato {
    private String nome = "";
    private String ddd = "";
    private String numeroTelefone = "";
    private String email = "";

    public Contato(String nome, String ddd, String numeroTelefone, String email){
        this.nome = nome;
        this.ddd = ddd;
        this.numeroTelefone = numeroTelefone;
        this.email = email;
    }

    public Contato(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
