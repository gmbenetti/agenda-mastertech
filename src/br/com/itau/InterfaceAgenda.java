package br.com.itau;

import java.util.Scanner;

import static br.com.itau.Constantes.*;


public class InterfaceAgenda {
    private int acao = 0;
    public InterfaceAgenda(){}
    Agenda agenda = new Agenda();

    public void viewInicio(){
        do{
            Scanner scanner = new Scanner(System.in);
            System.out.println("\nDigite a opção desejada");
            System.out.println("   1 - Inclusão");
            System.out.println("   2 - Remover");
            System.out.println("   3 - Imprimir");
            System.out.print("   9 - Sair   ");
            acao = scanner.nextInt();

            if (acao == INCLUIR_CONTATO){
                viewSolicitarDadosContato();
            }else if (acao == REMOVER_CONTATO){
                viewBuscaContato(acao);
            }else if (acao == IMPRIMIR_CONTATO){
                viewBuscaContato(acao);
            }
        } while (acao != SAIR);
    }

    public void viewBuscaContato(int acao){
        Scanner scanner = new Scanner(System.in);
        int busca = 0;

        System.out.println("\nBuscar por: ");
        System.out.println("   4 - email");
        System.out.print("   5 - número de telefone   ");
        busca = scanner.nextInt();

        if(busca == BUSCAR_POR_EMAIL){
            if (acao == REMOVER_CONTATO) viewRemoveContatoPorEmail();
            if (acao == IMPRIMIR_CONTATO) viewImprimeContatoPorEmail();
        }else if (busca == BUSCAR_POR_TELEFONE){
            if (acao == REMOVER_CONTATO) viewRemoveContatoPorTelefone();
            if (acao == IMPRIMIR_CONTATO) viewImprimeContatoPorTelefone();
        }else{
            System.out.println("Opção inválida, retornando para o menu inicial\n");
        }
    }

    public void viewImprimeContatoPorTelefone(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("\nDigite o telefone para impressão");
        String telefone = scanner.next();

        System.out.println("       Nome: " + agenda.buscaContatoPorTelefone(telefone).getNome());
        System.out.println("       DDD: " + agenda.buscaContatoPorTelefone(telefone).getDdd());
        System.out.println("       Número do Telefone: " + agenda.buscaContatoPorTelefone(telefone).getNumeroTelefone());
        System.out.println("       e-mail: " + agenda.buscaContatoPorTelefone(telefone).getEmail());
    }

    public void viewImprimeContatoPorEmail(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nDigite o email para impressão  " );
        String email = scanner.next();

        System.out.println("       Nome: " + agenda.buscaContatoPorEmail(email).getNome());
        System.out.println("       DDD: " + agenda.buscaContatoPorEmail(email).getDdd());
        System.out.println("       Número do Telefone: " + agenda.buscaContatoPorEmail(email).getNumeroTelefone());
        System.out.println("       e-mail: " + agenda.buscaContatoPorEmail(email).getEmail());
    }

    public void viewRemoveContatoPorEmail(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nDigite o email para remover o contato  " );
        String email = scanner.next();

        int contatoRemovido = agenda.removeContatoPorEmail(email);

        if(contatoRemovido == CONTATO_NAO_ENCONTRADO) {
            System.out.println("O email " + email + " não pertence à nenhum contato\n");
        }else{
            System.out.println("Contato removido com sucesso\n");
        }
    }


    public void viewRemoveContatoPorTelefone(){
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nDigite o telefone para remover o contato  " );
        String telefone = scanner.next();

        int contatoRemovido = agenda.removeContatoPorNumeroTelefone(telefone);

        if(contatoRemovido == CONTATO_NAO_ENCONTRADO) {
            System.out.println("O número de telefone " + telefone + " não pertence à nenhum contato\n");
        }else{
            System.out.println("Contato removido com sucesso\n");
        }
    }

    public void viewSolicitarDadosContato(){
        int quantidadeContato = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.print("\nQuantos contatos deseja adicionar? ");
        quantidadeContato = scanner.nextInt();

        if (quantidadeContato > 0){

            for(int i = 1; i <= quantidadeContato; i++){
                Contato contato = new Contato();
                System.out.println("\nInsira os dados do contato " + i);
                System.out.print("Nome: " );
                contato.setNome(scanner.next());

                System.out.print("DDD: ");
                contato.setDdd(scanner.next());

                System.out.print("Telefone: ");
                contato.setNumeroTelefone(scanner.next());

                System.out.print("e-mail: ");
                contato.setEmail(scanner.next());

                agenda.adicionaContato(contato);
                System.out.println("Contato adicionado com sucesso\n");
            }
        }
    }

}
