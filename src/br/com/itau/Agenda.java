package br.com.itau;


import java.util.ArrayList;
import static br.com.itau.Constantes.*;

public class Agenda {

    ArrayList<Contato> agenda = new ArrayList();

    public Agenda(){}

    public void adicionaContato(Contato contato){
        this.agenda.add(contato);
    }

    public Contato buscaContatoPorTelefone(String telefone){
        return this.agenda.get(getIndiceContatoPorTelefone(telefone));
    }

    private int getIndiceContatoPorTelefone(String telefone){
        for(int i = 0; i <this.agenda.size(); i++){
            if (this.agenda.get(i).getNumeroTelefone().equals(telefone)){
                return i;
            }
        }
        return CONTATO_NAO_ENCONTRADO;
    }

    public Contato buscaContatoPorEmail(String email){
        return this.agenda.get(getIndiceContatoPorEmail(email));
    }

    private int getIndiceContatoPorEmail(String email){
        for(int i = 0; i < this.agenda.size(); i++){
            if (this.agenda.get(i).getEmail().equals(email)){
                return i;
            }
        }
        return CONTATO_NAO_ENCONTRADO;
    }

    public int removeContatoPorEmail(String email){
        for(int i = 0; i < this.agenda.size(); i++){
            if (this.agenda.get(i).getEmail().equals(email)){
                this.agenda.remove(i);
                return i;
            }
        }
        return CONTATO_NAO_ENCONTRADO;
    }

    public int removeContatoPorNumeroTelefone(String telefone){
        for(int i = 0; i < this.agenda.size(); i++){
            if (this.agenda.get(i).getNumeroTelefone().equals(telefone)){
                this.agenda.remove(i);
            return i;
            }
        }
        return CONTATO_NAO_ENCONTRADO;
    }}
