package br.com.itau;

public class Constantes {

    public static final int INCLUIR_CONTATO = 1;
    public static final int REMOVER_CONTATO = 2;
    public static final int IMPRIMIR_CONTATO = 3;
    public static final int BUSCAR_POR_EMAIL = 4;
    public static final int BUSCAR_POR_TELEFONE = 5;
    public static final int SAIR = 9;
    public static final int CONTATO_NAO_ENCONTRADO = -1;

}
